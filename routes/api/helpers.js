var util = require('util');
var _ = require('underscore');
var log = require('libs/log')(module);

var makeMatch = exports.makeMatch = function (match) {
    var resultQuery = {};
    var field = match['f'];

    var requiredFieldsCount = {};
    requiredFieldsCount[field] = {$gte: 1};
    for (var subField in match) {
        if (subField == 'meta') continue;
        if (match[subField] instanceof Object && '$all' in match[subField]) {
            requiredFieldsCount[field] = {$gte: match[subField]['$all'].length};
            resultQuery[subField] = {'$in': match[subField]['$all']};
        } else {
            resultQuery[subField] = match[subField];
        }
    }
    return {query: resultQuery, requiredFieldsCount: requiredFieldsCount};

};

var getIdsQuery = exports.getIdsQuery = function (match) {
    var idAggregateQuery = [];
    var matchMade = makeMatch(match);
    idAggregateQuery.push({$match: matchMade['query']});

    var groupQuery = {_id: '$i'};
    groupQuery[match['f']] = {$sum: 1};
    groupQuery['fields'] = {$push: '$_id'};
    idAggregateQuery.push({'$group': groupQuery});

    if (Object.keys(matchMade['requiredFieldsCount']).length) {
        idAggregateQuery.push({$match: matchMade['requiredFieldsCount']});
    }

    idAggregateQuery.push({$group: { _id: null, ids: {$addToSet: '$_id'}, fields: {$push:'$fields'}}});

    log.info(util.inspect(idAggregateQuery, {depth: 8, colors: true}));
    return idAggregateQuery;
};

var getNullIdsQuery = exports.getNullIdsQuery = function (match) {
    var idAggregateQuery = [];
    var fieldMatch = {f: {$in: ['type', match['f']]}};
    idAggregateQuery.push({$match: fieldMatch});

    var groupQuery = {_id: '$i' };
    groupQuery[match['f']] = { '$sum': { '$cond': [{ '$eq': [ '$f', match['f'] ] },1,0] } };
    idAggregateQuery.push({'$group': groupQuery});

    var checkFieldsCount = {};
    checkFieldsCount[match['f']] = 0;
    idAggregateQuery.push({$match: checkFieldsCount});
    idAggregateQuery.push({$group: { _id: null, ids: {$addToSet: '$_id'}}});

    log.info(util.inspect(idAggregateQuery, {depth: 8, colors: true}));
    return idAggregateQuery;
};

function getMatch(ids, fieldsData, sort, deep) {
    var match;
    if (ids) {
        match = {i: {$in: ids}};
    } else {
        match = {};
    }
    var typeMatch;

    var fields = _.pluck(fieldsData, 'f');
    if (fields.indexOf('type') < 0) {
        typeMatch = {$in: ['type']};
        typeMatch['$in'].push.apply(typeMatch['$in'], fields);
    } else {
        typeMatch = {$in: fields};
    }
    updateSortType(typeMatch['$in'], sort, deep);
    match['f'] = typeMatch;
    return match;
}

function updateSortType(typeMatch, sort, deep) {
    for (var index = 0; sort[index] && index < deep; index++) {
        var currentSort = sort[index];
        var sortArray = currentSort[0].split('.');
        if (sortArray.length > 1) {
            if (typeMatch.indexOf(sortArray[0]) < 0) {
                typeMatch.push(sortArray[0]);
            }
        }
    }
}

function getGroupQuery(fields, sort, deep) {
    var groupQuery = {};
    groupQuery['_id'] = '$i';
    updateGroupQueryBySort(sort, groupQuery, deep);
    return groupQuery;
}

function updateGroupQueryBySort(sort, groupQuery, deep) {
    for (var index = 0; sort[index] && index < deep; index++) {
        var currentSort = sort[index];
        var sortArray = currentSort[0].split('.');
        if (sortArray.length > 1) {
            groupQuery[index] = {$max: {$cond: [ {$eq:['$f', sortArray[0]]}, '$v.' + sortArray[1], null]}}
        } else {
            groupQuery[index] = {$max: {$cond: [ {$eq:['$f', 'type']}, '$' + sort[0][0], null]}};
        }
    }
}

function getSortQuery(sort, deep) {
    var res = {};
    for (var index = 0; sort[index] && index < deep; index++) {
        var currentSort = sort[index];
        res[index] = currentSort[1];
    }
    return res;
}

var getSortedIdsQuery = exports.getSortedIdsQuery = function (ids, fields, match, sort, limit, skip) {
    var sortAggregateQuery = [];

    var sortArray = sort[0][0].split('.');
    var sortField = sortArray[0];
    if (ids != null) {
        var matchFields = {$or: [{'f': 'type'}, {'f': sortField}], i: {$in: ids}};
        if (sortField in fields) {
            matchFields['$or'][1]['_id'] = {$in: fields[sortField]}
        }
    } else {
        var matchFields = {$or: [{'f': 'type'}]};
    }

    sortAggregateQuery.push({$match: matchFields});

    var groupQuery = getGroupQuery([], sort, 1);
    sortAggregateQuery.push({'$group': groupQuery});

    var sortQuery = getSortQuery(sort, 1);
    sortAggregateQuery.push({'$sort': sortQuery});

    sortAggregateQuery.push({$limit: limit});
    sortAggregateQuery.push({$skip: skip});

    sortAggregateQuery.push({$group: { _id: null, ids: {$addToSet: '$_id'}}});

    log.info(util.inspect(sortAggregateQuery, {depth: 8, colors: true}));
    return sortAggregateQuery;
};

function removeNull(fieldsArray) {
    var res = {};
    res['_id'] = 1;
    for (var index in fieldsArray) {
        var field = fieldsArray[index];
        res[field['name']] = {$setDifference: ['$'+field['name'], [null]]};
    }
    return res;
}

var getItemsQuery = exports.getItemsQuery = function (ids, fieldsIDs, match, fields, sort) {
    var itemsAggregateQuery = [];

    var match = getMatch(ids, fields, sort, 2);
    itemsAggregateQuery.push({$match: match});

    var groupQuery = getGroupQuery(fields, sort, 2);
    itemsAggregateQuery.push({'$group': groupQuery});

    var sortQuery = getSortQuery(sort, 2);
    if (Object.keys(sortQuery).length) {
        itemsAggregateQuery.push({'$sort': sortQuery});
    }

    var removeNullProject = removeNull(fields);
    itemsAggregateQuery.push({'$project': removeNullProject});

    log.info(util.inspect(itemsAggregateQuery, {depth: 10, colors: true}));
    return itemsAggregateQuery;
};