var mongoose = require('libs/mongoose');
var util = require('util');
var log = require('libs/log')(module);
var async = require('async');
var helpers = require('routes/api/helpers');
var intersec = require('libs/intersec');
var _ = require('underscore');

var getIds = function(currentMatch, collection, name, callback) {
    async.parallel([
        function (matchCallback) {
            var currentAggregateQuery = helpers.getIdsQuery(currentMatch);
            collection.aggregate(currentAggregateQuery, matchCallback);
        },
        function (nullCallback) {
            if (currentMatch['meta'] && 'null' in currentMatch['meta']) {
                var nullQuery = helpers.getNullIdsQuery(currentMatch);
                collection.aggregate(nullQuery, nullCallback);
            } else {
                nullCallback(null, null);
            }
        }
    ], function (err, results) {
        var currentIds = [];
        var fields = [];
        var matchResult = results[0];
        if (matchResult.length > 0) {
            currentIds = matchResult[0]['ids'];
            fields = _.flatten(matchResult[0]['fields']);
        }
        var nullResult = results[1];
        if (nullResult != null && nullResult.length > 0) {
            currentIds.push.apply(currentIds, nullResult[0]['ids']);
        }
        var temp = {};
        temp[name] = fields;
        callback(err, {ids: currentIds, fields: temp});
    })
};

var setParameters = function(currentMatch, collection, name) {
    return function (callback) {
        getIds(currentMatch, collection, name, callback);
    }
};

var getMatchedIds = exports.getNextIdsQuery = function (matchDict, callback) {
    var collection = mongoose.connection.db.collection('fields');
    var tasks = [];
    for (var name in matchDict) {
        for (var currentIndex in matchDict[name]) {
            var currentMatch = matchDict[name][currentIndex];
            tasks.push(setParameters(currentMatch, collection, name));
        }
    }
    async.parallel(
        tasks,
        function (err, results) {
            callback(err, {ids: intersec(_.pluck(results, 'ids')),
                fields: _.groupBy(_.pluck(results, 'fields'), function(data){ return Object.keys(data)})});
        }
    );
};

function timerLog(startTimer, totalTime, timerId){
    var totalDiff = new Date() - startTimer;
    var sum = totalTime.reduce(function(pv, cv) { return pv + cv; }, 0);
    var currentDiff = totalDiff - sum;
    log.info(util.format('Query #: %s Query time: %s', timerId, currentDiff));
    return currentDiff;
}

var getItemsList = exports.getItemsList = function (query, callback){
    var collection = mongoose.connection.db.collection('fields');
    var startTimer = new Date();
    var totalTime = [0];
    var timerId = Math.random();
    var pageCount;
    var fields;
    log.info(util.format('Query #: %s', timerId));

    async.waterfall([
        function(callback) {
            if (Object.keys(query.match).length > 0) {
                getMatchedIds(query.match, callback);
            } else {
                callback(null, null);
            }
        },
        function(ids, callback) {
            if (ids) {
                pageCount = Math.ceil(ids.length/query.limit);
                callback(null, ids);
            } else {
                collection.count({f: "type"}, function(err, result) {
                    pageCount = Math.ceil(result/query.limit);
                    callback(err, ids);
                });
            }
        },
        function(results, callback) {
            totalTime.push(timerLog(startTimer, totalTime, timerId));
            fields = ids && results['fields'];
            var ids = results && results['ids'];
            var sortAggregateQuery = helpers.getSortedIdsQuery(ids, fields, query.match, query.sort, query.limit, query.skip);
            collection.aggregate(sortAggregateQuery, function(err, results) {
                callback(err, results);
            });
        },
        function(results, callback) {
            totalTime.push(timerLog(startTimer, totalTime, timerId));
            if (results.length > 0) {
                var itemsAggregateQuery = helpers.getItemsQuery(results[0]['ids'], fields, query.match, query.fields, query.sort);
                collection.aggregate(itemsAggregateQuery, callback);
            } else {
                callback(null, results);
            }
        }
    ], function (err, results) {
        if (err) {
            callback(err);
        } else {
            totalTime.push(timerLog(startTimer, totalTime, timerId));
            log.info(util.format('Query #: %s Total time: %s', timerId, totalTime));
            callback(null, {meta:{pages:pageCount}, data:results});
        }

    });
};

var getParameters = exports.getParameters = function (query) {
    var result = {};
    result['match'] = JSON.parse(query.match || "[]");
    result['fields'] = JSON.parse(query.fields || "[]");
    result['sort'] = JSON.parse(query.sort || '[["i",-1]]');
    result['limit'] = +query.limit || 100;
    result['skip'] = +query.skip || 0;

    return result;
};