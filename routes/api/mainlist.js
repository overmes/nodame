var url = require('url');
var log = require('libs/log')(module);
var mainlistutils = require('./mainlistutils');

exports.get = function(req, res, next) {
    try {
        var urlParsed = url.parse(req.url, true);
        var parameters = mainlistutils.getParameters(urlParsed.query);
        log.info(urlParsed);
        mainlistutils.getItemsList(parameters, function(err, results){
            if (err) {
                next(err);
            } else {
                res.send(results);
            }

        });
    } catch(err){
        next(err);
    }

};