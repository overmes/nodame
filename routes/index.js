var HttpError = require('error').HttpError;

module.exports = function(app) {
    app.get('/', require('./frontpage').get);
    app.get('/api/mainlist', require('./api/mainlist').get);

};
