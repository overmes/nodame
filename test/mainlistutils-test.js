var config = require('config');
config.set('mongoose:uri', "mongodb://localhost/nodetest");
var _ = require('underscore');

var util = require('util');
var async = require('async');
var url = require('url');
var mongoose = require('libs/mongoose');
var mainlistutils = require('routes/api/mainlistutils');
var assert = require('chai').assert;

var helpers = require('routes/api/helpers');

function open(callback) {
    mongoose.connection.on('open', callback);
}

function dropDatabase(callback) {
    var db = mongoose.connection.db;
    db.dropDatabase(callback);
}

function requireModels(callback) {
    require('models/field');

    async.each(Object.keys(mongoose.models), function(modelName, callback) {
        mongoose.models[modelName].ensureIndexes(callback);
    }, callback);
}

var fields = [
    { i: 0, v: { user: 1, date: 1, score: 1, a: 'Manga' }, f: 'type' },
    { i: 0, v: { user: 1, date: 1, score: 1, a: 1 }, f: 'scores' },
    { i: 0, v: { user: 2, date: 1, score: 1, a: 3 }, f: 'scores' },
    { i: 0, v: { user: 1, date: 1, score: 1, a: 'Romance', b: 0.1 }, f: 'genres' },

    { i: 1, v: { user: 1, date: 1, score: 1, a: 'OVA' }, f: 'type' },
    { i: 1, v: { user: 1, date: 1, score: 1, a: 2 }, f: 'scores' },
    { i: 1, v: { user: 2, date: 1, score: 1, a: 2 }, f: 'scores' },
    { i: 1, v: { user: 1, date: 1, score: 1, a: 'Action', b: 0.5 }, f: 'genres' },

    { i: 2, v: { user: 1, date: 1, score: 1, a: 'TV' }, f: 'type' },
    { i: 2, v: { user: 1, date: 1, score: 1, a: 1 }, f: 'date' },
    { i: 2, v: { user: 1, date: 1, score: 1, a: 4 }, f: 'scores' },
    { i: 2, v: { user: 2, date: 1, score: 1, a: 1 }, f: 'scores' },
    { i: 2, v: { user: 1, date: 1, score: 1, a: 'Comedy', b: 0.2 }, f: 'genres' },

    { i: 3, v: { user: 1, date: 1, score: 1, a: 'TV' }, f: 'type' },
    { i: 3, v: { user: 1, date: 1, score: 1, a: 2 }, f: 'date' },
    { i: 3, v: { user: 1, date: 1, score: 1, a: 4 }, f: 'scores' },
    { i: 3, v: { user: 2, date: 1, score: 1, a: 5 }, f: 'scores' },
    { i: 3, v: { user: 1, date: 1, score: 1, a: 'Comedy', b: 0.3 }, f: 'genres' },
    { i: 3, v: { user: 1, date: 1, score: 1, a: 'Action', b: 0.5 }, f: 'genres' },

    { i: 4, user: 1, date: 1, score: 1, v: { a: 'TV' }, f: 'type' }
];

function createItems(callback) {
    var collection = mongoose.connection.db.collection('fields');
    async.each(fields, function(fieldData, callback) {
        collection.insert(fieldData, callback);
    }, callback);
}

describe('Test main list', function(){
    before(function (callback) {
        async.series([
            open,
            dropDatabase,
            requireModels,
            createItems
        ], function(err) {
            if(err) throw new Error(err);
            callback();
        });
    });

    after(function (callback) {
        dropDatabase(function(){
            mongoose.disconnect();
            callback();
        });

    });
    describe('test main list api', function(){
        it('test empty query', function(done){
            var urlParsed = url.parse('/api/mainlist', true);
            var parameters = mainlistutils.getParameters(urlParsed.query);
            mainlistutils.getItemsList(parameters, function(err, results){
                console.log(util.inspect(results, {depth: 10, colors: true}));
                if(err) throw new Error(err);
                var data = results['data'];
                assert.lengthOf(data, 5, "length");
                done();
            });
        });
        it('test empty result', function(done){
            var urlParsed = url.parse('/api/mainlist?match={"1":[{"f": "type", "v.a":{"$in":["aaa"]}}]}', true);
            var parameters = mainlistutils.getParameters(urlParsed.query);
            mainlistutils.getItemsList(parameters, function(err, results){
                console.log(util.inspect(results, {depth: 10, colors: true}));
                if(err) throw new Error(err);
                var data = results['data'];
                assert.lengthOf(data, 0, "length");
                done();
            });
        });
        it('test page count', function(done){
            var urlParsed = url.parse('/api/mainlist?limit=2', true);
            var parameters = mainlistutils.getParameters(urlParsed.query);
            mainlistutils.getItemsList(parameters, function(err, results){
                console.log(util.inspect(results, {depth: 10, colors: true}));
                if(err) throw new Error(err);
                assert(results['meta']['pages'], 3, "length");
                done();
            });
        });
        it('test fields', function(done){
            var urlParsed = url.parse('/api/mainlist?fields=["scores", "type"]', true);
            var parameters = mainlistutils.getParameters(urlParsed.query);
            mainlistutils.getItemsList(parameters, function(err, results){
                if(err) throw new Error(err);
                console.log(util.inspect(results, {depth: 10, colors: true}));
                var data = results['data'];
                assert(data.length > 0, "has result");
                for (var index in data) {
                    var obj = data[index];
                    assert.notProperty(obj, "genres", "has not genres");
                    assert.property(obj, "scores", "has scores");
                    if (obj['scores'].length) {
                        assert.deepProperty(obj, "scores.0.a", "has scores a");
                    }
                    assert.property(obj, "type", "has type");

                    if (obj['type'].length) {
                        assert.deepProperty(obj, "type.0.a", "has type a");
                    }
                }
                done();
            });
        });

        it('test in match', function(done){
            var urlParsed = url.parse('/api/mainlist?match=[{"1": {"f": "type", "v.a":{"$in":["Manga","OVA"]}}}]&fields=[{"f":"type", "name": "type"}]', true);
            var parameters = mainlistutils.getParameters(urlParsed.query);
            mainlistutils.getItemsList(parameters, function(err, results){
                if(err) throw new Error(err);
                console.log(util.inspect(results, {depth: 10, colors: true}));
                var data = results['data'];
                assert.lengthOf(data, 2, "has 2 results");
                for(var index in data) {
                    var obj = data[index];
                    assert.include(['Manga', 'OVA'], obj['type'][0]['a'], "has proper type");
                }
                done();
            });
        });

        it('test all match', function(done){
            var urlParsed = url.parse('/api/mainlist?match=[{"1": {"f": "genres", "v.a":{"$all":["Comedy","Action"]}}}]&fields=[{"f":"genres", "name": "genres"}]', true);
            var parameters = mainlistutils.getParameters(urlParsed.query);
            mainlistutils.getItemsList(parameters, function(err, results){
                if(err) throw new Error(err);
                console.log(util.inspect(results, {depth: 10, colors: true}));
                var data = results['data'];
                assert.lengthOf(data, 1, "has 1 results");
                for(var index in data) {
                    var obj = data[index];
                    assert.include(['Comedy', 'Action'], obj['genres'][0]['a'], "has proper genre");
                    assert.include(['Comedy', 'Action'], obj['genres'][1]['a'], "has proper genre");
                }
                done();
            });
        });

        it('test range match', function(done){
            var urlParsed = url.parse('/api/mainlist?match=[{"1": {"f": "scores", "v.a":{"$gte":2,"$lte":3}}}]&fields=[{"f":"scores", "name": "scores"}]', true);
            var parameters = mainlistutils.getParameters(urlParsed.query);
            mainlistutils.getItemsList(parameters, function(err, results){
                if(err) throw new Error(err);
                console.log(util.inspect(results, {depth: 10, colors: true}));
                var data = results['data'];
                assert.lengthOf(data, 2, "has 2 results");
                for(var index in data) {
                    var obj = data[index];
                    assert(_.max(_.pluck(obj['scores'], 'a')) >= 2, "has proper scores");
                    assert(_.max(_.pluck(obj['scores'], 'a')) <= 3, "has proper scores");
                }
                done();
            });
        });

        it('test range match b', function(done){
            var urlParsed = url.parse('/api/mainlist?match=[{"f": "genres", "v.b":{"$gte":0.3}, "meta": {"name": "1"}}]&fields=[{"f":"genres", "name": "genres"}]', true);
            var parameters = mainlistutils.getParameters(urlParsed.query);
            mainlistutils.getItemsList(parameters, function(err, results){
                if(err) throw new Error(err);
                console.log(util.inspect(results, {depth: 10, colors: true}));
                var data = results['data'];
                assert.lengthOf(data, 2, "has 2 results");
                for(var index in data) {
                    var obj = data[index]['genres'];
                    for (var sub in obj) {
                        assert(obj[sub]['b'] >= 0.3, "has proper scores");
                    }
                }
                done();
            });
        });

        it('test range match b & a', function(done){
            var urlParsed = url.parse('/api/mainlist?match=[{"f": "genres", "v.b":{"$gte":0.3}, "meta": {"name": "1"}}, {"f": "genres", "v.a":{"$in":["Comedy"]}, "meta": {"name": "2"}}]&fields=[{"f":"genres", "name": "genres"}]', true);
            var parameters = mainlistutils.getParameters(urlParsed.query);
            mainlistutils.getItemsList(parameters, function(err, results){
                if(err) throw new Error(err);
                console.log(util.inspect(results, {depth: 10, colors: true}));
                var data = results['data'];
                assert.lengthOf(data, 1, "has 2 results");
                for(var index in data) {
                    var obj = data[index]['genres'];
                    for (var sub in obj) {
                        assert(obj[sub]['b'] >= 0.3, "has proper scores");
                    }
                    assert.include(_.pluck(data[index]['genres'], 'a'), 'Comedy', "has proper genre");
                }
                done();
            });
        });

        it('test user match', function(done){
            var urlParsed = url.parse('/api/mainlist?match=[{"f": "scores", "v.a":{"$gte":2}, "v.user": 2, "meta": {"name": "1"}}]&fields=["1"]', true);
            var parameters = mainlistutils.getParameters(urlParsed.query);
            mainlistutils.getItemsList(parameters, function(err, results){
                if(err) throw new Error(err);
                console.log(util.inspect(results, {depth: 10, colors: true}));
                var data = results['data'];
                assert.lengthOf(data, 3, "has 3 results");
                for(var index in data) {
                    var obj = data[index]['scores2'];
                    var scoresUser = _.union(_.pluck(obj, 'user'), [2]);
                    assert(scoresUser == 2, "has proper user");
                    assert(_.max(_.pluck(data[index]['scores2'], 'a')) >= 2, "has proper score");
                }
                done();
            });
        });

        it('test multi match', function(done){
            var urlParsed = url.parse('/api/mainlist?match=[{"f": "scores", "v.a":{"$gte":1,"$lte":3}, "meta": {"name": "1"}},{"f": "type", "v.a":{"$eq":"Manga"}, "meta": {"name": "1"}}]&' +
                'fields=[{"f":"scores", "name": "scores"}, {"f":"type", "name": "type"}]', true);
            var parameters = mainlistutils.getParameters(urlParsed.query);
            mainlistutils.getItemsList(parameters, function(err, results){
                if(err) throw new Error(err);
                console.log(util.inspect(results, {depth: 10, colors: true}));
                var data = results['data'];
                assert.lengthOf(data, 1, "has 2 results");
                for(var index in data) {
                    var obj = data[index];
                    assert(obj['scores'][0]['a'] >= 1, "has proper scores");
                    assert(obj['scores'][0]['a'] <= 3, "has proper scores");
                    assert(obj['type'][0]['a'] == "Manga", "has proper type");
                }
                done();
            });
        });

        it('test or null match', function(done){
            var urlParsed = url.parse('/api/mainlist?match=[{"f": "scores", "v.a":{"$gte":1,"$lte":3}, "meta": {"null":true, "name": "1"}}]&fields=["1"]', true);
            var parameters = mainlistutils.getParameters(urlParsed.query);
            mainlistutils.getItemsList(parameters, function(err, results){
                if(err) throw new Error(err);
                console.log(util.inspect(results, {depth: 10, colors: true}));
                var data = results['data'];
                assert.lengthOf(data, 4, "has 4 results");
                for(var index in data) {
                    var obj = data[index];

                    if (obj['scores'].length) {
                        assert(_.max(_.pluck(obj['1'],['a'])) >= 1, "has proper scores");
                        assert(_.max(_.pluck(obj['1'],['a'])) <= 3, "has proper scores");
                    } else {
                        assert(obj['_id'] == 4, "has null scores");
                    }
                }
                done();
            });
        });

        it('test multi match and null match', function(done){
            var urlParsed = url.parse('/api/mainlist?match=[{"scores":{"v.a":{"$gte":1,"$lte":3}, "null":true}},{"type":{"v.a":{"$in":["TV"]}}}]&' +
                'fields=[{"f":"scores", "name": "scores"},{"f":"type", "name": "type"}]', true);
            var parameters = mainlistutils.getParameters(urlParsed.query);
            mainlistutils.getItemsList(parameters, function(err, results){
                if(err) throw new Error(err);
                console.log(util.inspect(results, {depth: 10, colors: true}));
                var data = results['data'];
                assert.lengthOf(data, 2, "has 2 results");
                for(var index in data) {
                    var obj = data[index];
                    assert(obj['type'][0]['a'] == "TV", "has proper type");
                    if (obj['scores'].length) {
                        assert(_.max(_.pluck(obj['scores'],['a'])) >= 1, "has proper scores");
                        assert(_.max(_.pluck(obj['scores'],['a'])) <= 3, "has proper scores");
                    } else {
                        assert(obj['_id'] == 4, "has null scores");
                    }
                }
                done();
            });
        });

        it('test simple sort descending', function(done){
            var urlParsed = url.parse('/api/mainlist?sort=[["scores.a",-1]]&fields=[{"f":"scores", "name": "scores"}]', true);
            var parameters = mainlistutils.getParameters(urlParsed.query);
            mainlistutils.getItemsList(parameters, function(err, results){
                if(err) throw new Error(err);
                console.log(util.inspect(results, {depth: 10, colors: true}));
                var data = results['data'];
                assert.lengthOf(data, 5, "has 4 results");
                var lastValue = 99999;
                for (var index in data) {
                    var obj = data[index];
                    if (obj['scores'].length > 0) {
                        assert(_.max(_.pluck(obj['scores'],['a'])) <= lastValue, "has score less then previous");
                        var lastValue = _.max(_.pluck(obj['scores'],['a']));
                    }
                }
                done();
            });
        });

        it('test simple sort descending b', function(done){
            var urlParsed = url.parse('/api/mainlist?sort=[["genres.b",-1]]&fields=[{"f":"genres", "name": "genres"}]', true);
            var parameters = mainlistutils.getParameters(urlParsed.query);
            mainlistutils.getItemsList(parameters, function(err, results){
                if(err) throw new Error(err);
                console.log(util.inspect(results, {depth: 10, colors: true}));
                var data = results['data'];
                assert.lengthOf(data, 5, "has 4 results");
                var lastValue = 99999;
                for (var index in data) {
                    var obj = data[index]['genres'];
                    for (var subIndex in obj) {
                        assert(obj[subIndex]['b'] <= lastValue, "has score less then previous");
                    }
                    var lastValue = _.max(_.pluck(obj, 'b'));
                }
                done();
            });
        });

        it('test simple sort ascending', function(done){
            var urlParsed = url.parse('/api/mainlist?sort=[["scores.a",1]]&fields=[{"f":"scores", "name": "scores"}]', true);
            var parameters = mainlistutils.getParameters(urlParsed.query);
            mainlistutils.getItemsList(parameters, function(err, results){
                if(err) throw new Error(err);
                console.log(util.inspect(results, {depth: 10, colors: true}));
                var data = results['data'];
                assert.lengthOf(data, 5, "has 4 results");
                var lastValue = -99999;
                for (var index in data) {
                    var obj = data[index];
                    if (obj['scores'].length > 0) {
                        assert(_.max(_.pluck(obj['scores'],['a'])) >= lastValue, "has score less then previous");
                        var lastValue = _.max(_.pluck(obj['scores'],['a']));
                    }
                }
                done();
            });
        });

        it('test simple sort by item', function(done){
            var urlParsed = url.parse('/api/mainlist?sort=[["i",-1]]', true);
            var parameters = mainlistutils.getParameters(urlParsed.query);
            mainlistutils.getItemsList(parameters, function(err, results){
                if(err) throw new Error(err);
                console.log(util.inspect(results, {depth: 10, colors: true}));
                var data = results['data'];
                assert.lengthOf(data, 5, "has 4 results");
                var lastValue = 99999;
                for (var index in data) {
                    var obj = data[index];
                    assert(obj['_id'] <= lastValue, "has score less then previous");
                    var lastValue = obj['_id'];
                }
                done();
            });
        });

        it('test multi sort descending', function(done){
            var urlParsed = url.parse('/api/mainlist?match=[{"scores":{"v.a":{"$eq":4}}}]&sort=[["scores.a",-1],["date.a", -1]]&' +
                'fields=[{"f":"scores", "name": "scores"},{"f":"date", "name": "date"}]', true);
            var parameters = mainlistutils.getParameters(urlParsed.query);
            mainlistutils.getItemsList(parameters, function(err, results){
                if(err) throw new Error(err);
                console.log(util.inspect(results, {depth: 10, colors: true}));
                var data = results['data'];
                assert.lengthOf(data, 2, "has 2 results");
                var lastValue = 99999;
                for (var index in data) {
                    var obj = data[index];
                    if (obj['date'].length > 0) {
                        assert(_.max(_.pluck(obj['date'],['a'])) <= lastValue, "has score less then previous");
                        var lastValue = _.max(_.pluck(obj['date'],['a']));
                    }
                }
                done();
            });
        });
        it('test multi sort ascending', function(done){
            var urlParsed = url.parse('/api/mainlist?match=[{"scores":{"v.a":{"$eq":4}}}]&sort=[["scores.a",-1, 1],["date.a", 1, 1]]&' +
                'fields=[{"f":"scores", "name": "scores", "v.user": 1},{"f":"date", "name": "date"}]', true);
            var parameters = mainlistutils.getParameters(urlParsed.query);
            mainlistutils.getItemsList(parameters, function(err, results){
                if(err) throw new Error(err);
                console.log(util.inspect(results, {depth: 10, colors: true}));
                var data = results['data'];
                assert.lengthOf(data, 2, "has 2 results");
                var lastValue = -99999;
                for (var index in data) {
                    var obj = data[index];
                    if (obj['date'].length > 0) {
                        assert(_.max(_.pluck(obj['date'],['a'])) >= lastValue, "has score less then previous");
                        var lastValue = _.max(_.pluck(obj['date'],['a']));
                    }
                }
                done();
            });
        });
    });
});