var crypto = require('crypto');
var async = require('async');
var util = require('util');

var mongoose = require('libs/mongoose'),
    Schema = mongoose.Schema;

var schema = new Schema({
    i: {type: Number},
    f: {type: String},
    v: {type: Schema.Types.Mixed}
});

exports.Field = mongoose.model('Field', schema);